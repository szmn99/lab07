package project;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;


public class PdfGenerator {
    private static String FILE = "./Resume.pdf";
    private static Font headerFont = new Font(Font.FontFamily.HELVETICA, 24, Font.BOLD);
    private static Font tableFont = new Font(Font.FontFamily.HELVETICA, 12, Font.NORMAL);

    protected static String getFilePath(){
        return FILE;
    }

    protected static void addMetaData(Document document) {
        document.addTitle("My first PDF");
        document.addSubject("Using iText");
        document.addAuthor("Szymon Kieronski");
        document.addCreator("Szymon Kieronski");
        document.addKeywords("Java, PDF, iText");
    }

    protected static void addHeader(Document document) throws DocumentException {
        Paragraph preface = new Paragraph();
        Paragraph header = new Paragraph("Resume", headerFont);
        preface.setAlignment(Element.ALIGN_CENTER);
        preface.add(header);
        addLineEnd(preface, 10);
        document.add(preface);
    }

    protected static void addContent(Document document) throws DocumentException {
        Paragraph tableParagraph = new Paragraph();
        createTable(tableParagraph);
        document.add(tableParagraph);
    }

    protected static void createTable(Paragraph para) {
        PdfPTable table = new PdfPTable(2);

        PdfPCell cell = new PdfPCell();
        cell.setPadding(5);
        cell.setPaddingBottom(10);

        cell.setPhrase(new Phrase("First Name", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Szymon", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Last Name", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Kieronski", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Education", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("2018-10 - Nowadays", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("Summary", tableFont));
        table.addCell(cell);
        cell.setPhrase(new Phrase("All work and no play makes Jack a dull boy \n " +
                "All work and no play makes Jack a dull boy", tableFont));
        table.addCell(cell);

        para.add(table);
    }

    protected static void addLineEnd(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}