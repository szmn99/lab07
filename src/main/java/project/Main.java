package project;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;

public class Main {

    public static void main (String[] args){
        try {
            Document doc = new Document();
            PdfWriter.getInstance(doc, new FileOutputStream(PdfGenerator.getFilePath()));
            doc.open();
            PdfGenerator.addMetaData(doc);
            PdfGenerator.addHeader(doc);
            PdfGenerator.addContent(doc);
            doc.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
